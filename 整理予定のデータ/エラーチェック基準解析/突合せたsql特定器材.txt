select 
mc.id チェック対象ID
,m.name チェック対象項目名
,t.type エラーチェックタイプ
,e.checkid
,mm.name
,e.checkstr チェック文字列

,case e.doubt when  '1' then '疑いは処方不可' when '0' then '' end 疑い可否

,lt.type 制限タイプ
,e.limcount 回数制限
,e.limdays 日数制限
,e.dayvolume 一日量
,e.limage 年齢制限

,e.UpdateDate

from 
MachineCheck mc
inner join McnCheckElm e 
on mc.id=e.baseid
inner join MachineMaster m
on e.baseid=m.id
left join machineCheckType t
on e.type=t.val
left join MachineMaster mm
on e.checkid=mm.id
left join LimitType lt
on e.LimType=lt.val

group by
 mc.id
,m.name
,e.checkid
,e.checkstr 

order by mc.id