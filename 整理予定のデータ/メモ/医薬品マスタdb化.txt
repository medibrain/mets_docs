DrugStandardCodeがある
genericDrugフラグが0　　→　オリジナル医薬品

薬価基準コードがoriginalに存在する場合、originalに薬価基準コードとオリジナル医薬品コードを追加

DrugMasterの医薬品IDでループ
DrugStandardCodeがある
genericDrugフラグ＝１	→　originalに薬価基準コードがあるか調査
							→ない場合　DrugMasterの医薬品コードとオリジナル医薬品コードをgenericsに登録
							→ある場合　DrugMasterの医薬品コードの行に、オリジナル医薬品コードを登録
