紐づけ方法

master_score_curr.db
以下のテーブルに、「BCS_SINRYOU_メディコムからもらうデータ」を取り込む
BCSD_kensa
BCSD_gazou
BCSJ_syoti2
BCSK_syoti1


以下のビューで紐づけている
medicom_gazou_medicalID_SickID
medicom_kensa_medicalID_SickID
medicom_syoti_medicalID_sickID


上記のビューから以下のテーブルを生成。
このテーブルのレコードが、画像・検査・処置と病名が紐付いている。
この中から査定確率の高い組み合わせを点検員が抽出する。
link_medicom_gazou
link_medicom_kensa
link_medicom_shoti

１．画面
	CSV取込画面
	テーブル編集画面
	
２．チェック項目として取り込む
	チェックルーチンの変更
	