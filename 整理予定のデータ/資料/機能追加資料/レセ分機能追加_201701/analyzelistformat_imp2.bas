Attribute VB_Name = "Module"
Option Explicit

Public Const CSV_CONF_BASIC As String = "confBasic.csv"
Public Const CSV_CONF_SICK As String = "confSick.csv"
Public Const CSV_CONF_DRUG As String = "confDrug.csv"
Dim curDir As String

Public Sub Main()
    Application.DisplayAlerts = False
    Application.ScreenUpdating = False
    
    curDir = ThisWorkbook.Path & "\"
    
    '重複受診
    If Dir(curDir & CSV_CONF_BASIC) <> "" Then Call FormatConfList
        
    '終了
    Application.DisplayAlerts = True
    Application.ScreenUpdating = True
    
    Call MsgBox("リスト作成しました")
    Application.Quit
    ThisWorkbook.Close savechanges:=False
    
End Sub

'■■■重複受診リスト
Public Sub FormatConfList()
    Const ROW_L_TITLE As Integer = 2
    
    Const COL_B_MARK       As String = "A"
    Const COL_B_NUMBER     As String = "B"
    Const COL_B_NAME       As String = "C"
    Const COL_B_BIRTH      As String = "D"
    Const COL_B_SCLASS     As String = "E"
    Const COL_B_SCLASSNAME As String = "F"
    Const COL_B_YM         As String = "G"
    Const COL_B_HOSID      As String = "H"
    Const COL_B_HOSNAME    As String = "I"
    Const COL_B_STOREID    As String = "J"
    Const COL_B_STORENAME  As String = "K"
    Const COL_B_POINT      As String = "L"
    
    Const COL_S_MARK       As String = "A"
    Const COL_S_NUMBER     As String = "B"
    Const COL_S_BIRTH      As String = "C"
    Const COL_S_SCLASS     As String = "D"
    Const COL_S_YM         As String = "E"
    Const COL_S_HOSID      As String = "F"
    Const COL_S_SICK       As String = "G"
    
    Const COL_D_MARK       As String = "A"
    Const COL_D_NUMBER     As String = "B"
    Const COL_D_BIRTH      As String = "C"
    Const COL_D_SCLASS     As String = "D"
    Const COL_D_YM         As String = "E"
    Const COL_D_HOSID      As String = "F"
    Const COL_D_EFFECT     As String = "G"
    Const COL_D_DRUG       As String = "H"
    
    Const COL_L_SEQ        As String = "A"
    Const COL_L_MARK       As String = "B"
    Const COL_L_NUMBER     As String = "C"
    Const COL_L_NAME       As String = "D"
    Const COL_L_BIRTH      As String = "E"
    Const COL_L_SCLASSNAME As String = "F"
    Const COL_L_YM         As String = "G"
    Const COL_L_HOS        As String = "H"
    Const COL_L_STORE      As String = "I"
    Const COL_L_POINT      As String = "J"
    Const COL_L_SICK       As String = "K"
    Const COL_L_EFFECT     As String = "L"
    Const COL_L_DRUG       As String = "M"
    
    Dim confSick As Boolean '傷病の比較の有無
    Dim confDrug As Boolean '医薬品の比較の有無
    
    Dim shBasic As Worksheet
    Dim shSick As Worksheet
    Dim shDrug As Worksheet
    
    Dim rRowB As Long
    Dim rRowS As Long
    Dim rRowD As Long
    Dim wRow As Long
    Dim preEffect As String
    
    '比較方法確認
    confSick = (Dir(curDir & CSV_CONF_SICK) <> "")
    confDrug = (Dir(curDir & CSV_CONF_DRUG) <> "")
    
    'レセ基本CSVインポート
    Sheets.Add
    ActiveSheet.Name = "basic"
    With ActiveSheet.QueryTables.Add(Connection:="TEXT;" & curDir & CSV_CONF_BASIC, Destination:=Range("$A$1"))
        .TextFilePlatform = 65001
        .TextFileStartRow = 1
        .TextFileParseType = xlDelimited
        .TextFileTextQualifier = xlTextQualifierDoubleQuote
        .TextFileCommaDelimiter = True
        .TextFileColumnDataTypes = Array(2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1)
        .Refresh BackgroundQuery:=False
    End With
    Set shBasic = ActiveSheet
    
    ProgStr "傷病CSVインポート"
    '傷病CSVインポート
    If confSick Then
        Sheets.Add
        ActiveSheet.Name = "sick"
        With ActiveSheet.QueryTables.Add(Connection:="TEXT;" & curDir & CSV_CONF_SICK, Destination:=Range("$A$1"))
            .TextFilePlatform = 65001
            .TextFileStartRow = 1
            .TextFileParseType = xlDelimited
            .TextFileTextQualifier = xlTextQualifierDoubleQuote
            .TextFileCommaDelimiter = True
            .TextFileColumnDataTypes = Array(2, 2, 2, 2, 2, 2, 2)
            .Refresh BackgroundQuery:=False
        End With
        Set shSick = ActiveSheet
    End If
    
    ProgStr "医薬品CSVインポート"
    '医薬品CSVインポート
    If confDrug Then
        Sheets.Add
        ActiveSheet.Name = "drug"
        With ActiveSheet.QueryTables.Add(Connection:="TEXT;" & curDir & CSV_CONF_DRUG, Destination:=Range("$A$1"))
            .TextFilePlatform = 65001
            .TextFileStartRow = 1
            .TextFileParseType = xlDelimited
            .TextFileTextQualifier = xlTextQualifierDoubleQuote
            .TextFileCommaDelimiter = True
            .TextFileColumnDataTypes = Array(2, 2, 2, 2, 2, 2, 2, 2)
            .Refresh BackgroundQuery:=False
        End With
        Set shDrug = ActiveSheet
    End If
    
    'リストフォーマットコピー
    Worksheets("重複受診").Copy
    
    'レセ基本情報から、合計点数までを入力する
    rRowB = 1: rRowS = 1: rRowD = 1: wRow = ROW_L_TITLE
    Do Until shBasic.Cells(rRowB, COL_B_YM) = ""
        'キー(記号、番号、生年月日、傷病分類、診療月、医療機関)が異なれば、次の行に行く
        If shBasic.Cells(rRowB, COL_B_MARK) <> Cells(wRow, COL_L_MARK) Or shBasic.Cells(rRowB, COL_B_NUMBER) <> Cells(wRow, COL_L_NUMBER) Or shBasic.Cells(rRowB, COL_B_BIRTH) <> Cells(wRow, COL_L_BIRTH) Or shBasic.Cells(rRowB, COL_B_SCLASSNAME) <> Cells(wRow, COL_L_SCLASSNAME) Or shBasic.Cells(rRowB, COL_B_YM) <> Cells(wRow, COL_L_YM) Or shBasic.Cells(rRowB, COL_B_HOSNAME) <> Cells(wRow, COL_L_HOS) Then
            wRow = wRow + 1
            Cells(wRow, COL_L_MARK) = shBasic.Cells(rRowB, COL_B_MARK)
            Cells(wRow, COL_L_NUMBER) = shBasic.Cells(rRowB, COL_B_NUMBER)
            Cells(wRow, COL_L_NAME) = shBasic.Cells(rRowB, COL_B_NAME)
            Cells(wRow, COL_L_BIRTH) = shBasic.Cells(rRowB, COL_B_BIRTH)
            Cells(wRow, COL_L_SCLASSNAME) = shBasic.Cells(rRowB, COL_B_SCLASSNAME)
            Cells(wRow, COL_L_YM) = shBasic.Cells(rRowB, COL_B_YM)
            Cells(wRow, COL_L_HOS) = shBasic.Cells(rRowB, COL_B_HOSNAME)
            Cells(wRow, COL_L_POINT) = shBasic.Cells(rRowB, COL_B_POINT)
        Else '同じキーで調剤レセが続く場合、薬局を追記し、点数を追加する
            Cells(wRow, COL_L_STORE) = Cells(wRow, COL_L_STORE) & IIf(Cells(wRow, COL_L_STORE) = "", "", vbCrLf) & shBasic.Cells(rRowB, COL_B_STORENAME)
            Cells(wRow, COL_L_POINT) = Cells(wRow, COL_L_POINT) + shBasic.Cells(rRowB, COL_B_POINT)
        End If
        
        'キーの最後に来たら、傷病、医薬品を編集する
        If shBasic.Cells(rRowB, COL_B_MARK) <> shBasic.Cells(rRowB + 1, COL_B_MARK) Or shBasic.Cells(rRowB, COL_B_NUMBER) <> shBasic.Cells(rRowB + 1, COL_B_NUMBER) Or shBasic.Cells(rRowB, COL_B_BIRTH) <> shBasic.Cells(rRowB + 1, COL_B_BIRTH) Or shBasic.Cells(rRowB, COL_B_SCLASS) <> shBasic.Cells(rRowB + 1, COL_B_SCLASS) Or shBasic.Cells(rRowB, COL_B_YM) <> shBasic.Cells(rRowB + 1, COL_B_YM) Or shBasic.Cells(rRowB, COL_B_HOSID) <> shBasic.Cells(rRowB + 1, COL_B_HOSID) Then
            'キーが変わるまで、傷病を編集
            If confSick Then
                Do Until shBasic.Cells(rRowB, COL_B_MARK) <> shSick.Cells(rRowS, COL_S_MARK) Or shBasic.Cells(rRowB, COL_B_NUMBER) <> shSick.Cells(rRowS, COL_S_NUMBER) Or shBasic.Cells(rRowB, COL_B_BIRTH) <> shSick.Cells(rRowS, COL_S_BIRTH) Or shBasic.Cells(rRowB, COL_B_SCLASS) <> shSick.Cells(rRowS, COL_S_SCLASS) Or shBasic.Cells(rRowB, COL_B_YM) <> shSick.Cells(rRowS, COL_S_YM) Or shBasic.Cells(rRowB, COL_B_HOSID) <> shSick.Cells(rRowS, COL_S_HOSID)
                    Cells(wRow, COL_L_SICK) = Cells(wRow, COL_L_SICK) & IIf(Cells(wRow, COL_L_SICK) = "", "", ",") & shSick.Cells(rRowS, COL_S_SICK)
                    rRowS = rRowS + 1
                Loop
            End If
            'キーが変わるまで、医薬品を編集
            If confDrug Then
                preEffect = ""
                Do Until shBasic.Cells(rRowB, COL_B_MARK) <> shDrug.Cells(rRowD, COL_D_MARK) Or shBasic.Cells(rRowB, COL_B_NUMBER) <> shDrug.Cells(rRowD, COL_D_NUMBER) Or shBasic.Cells(rRowB, COL_B_BIRTH) <> shDrug.Cells(rRowD, COL_D_BIRTH) Or shBasic.Cells(rRowB, COL_B_SCLASS) <> shDrug.Cells(rRowD, COL_D_SCLASS) Or shBasic.Cells(rRowB, COL_B_YM) <> shDrug.Cells(rRowD, COL_D_YM) Or shBasic.Cells(rRowB, COL_B_HOSID) <> shDrug.Cells(rRowD, COL_D_HOSID)
                    If preEffect <> shDrug.Cells(rRowD, COL_D_EFFECT) Then
                        Cells(wRow, COL_L_EFFECT) = Cells(wRow, COL_L_EFFECT) & IIf(preEffect = "", "", ",") & shDrug.Cells(rRowD, COL_D_EFFECT)
                        preEffect = shDrug.Cells(rRowD, COL_D_EFFECT)
                    End If
                    Cells(wRow, COL_L_DRUG) = Cells(wRow, COL_L_DRUG) & IIf(Cells(wRow, COL_L_DRUG) = "", "", ",") & shDrug.Cells(rRowD, COL_D_DRUG)
                    rRowD = rRowD + 1
                Loop
            End If
        
        End If
        rRowB = rRowB + 1
        
        Prog rRowB
    Loop
    
    '連番編集・書式調整
    Cells(ROW_L_TITLE + 1, COL_L_SEQ) = 1
    wRow = ROW_L_TITLE + 2
    '人(記号、番号、生年月日)ごとに連番を振る
    Do Until Cells(wRow, COL_L_YM) = ""
        If Cells(wRow - 1, COL_L_MARK) <> Cells(wRow, COL_L_MARK) Or Cells(wRow - 1, COL_L_NUMBER) <> Cells(wRow, COL_L_NUMBER) Or Cells(wRow - 1, COL_L_BIRTH) <> Cells(wRow, COL_L_BIRTH) Then
            Cells(wRow, COL_L_SEQ) = Cells(wRow - 1, COL_L_SEQ) + 1
        Else
            Cells(wRow, COL_L_SEQ) = Cells(wRow - 1, COL_L_SEQ)
        End If
        wRow = wRow + 1
        
        Prog wRow
    Loop
    
    
    If wRow > 5000 Then
    
        '一気にやるとエラーが起きる場合があるので刻んでやる
        Rows(ROW_L_TITLE + 1).Copy
    
        Dim from As Long 'ペースト開始行
        Dim torow As Long 'ペースト終了行
        from = ROW_L_TITLE + 1
        torow = wRow / 10 - 1
        
        Dim cnt As Long 'ループカウンタ
        cnt = 1
        
        Do
            'ペースト
            Rows(from & ":" & torow - 1).PasteSpecial Paste:=xlPasteFormats
            
            
            cnt = cnt + 1
            from = torow + 1 '終わり＋１を開始とする
            torow = (wRow / 10 - 1) * cnt '(wRow / 10-1)xループ回数
            
            Prog cnt
        Loop Until wRow > torow
        
        
        
        Application.StatusBar = False
    Else
    

        Rows(ROW_L_TITLE + 1).Copy
        Rows(ROW_L_TITLE + 1 & ":" & wRow - 1).PasteSpecial Paste:=xlPasteFormats
        
    End If
    
    Application.CutCopyMode = False
    With Range(Cells(wRow - 1, COL_L_SEQ), Cells(wRow - 1, COL_L_DRUG)).Borders(xlEdgeBottom)
        .LineStyle = xlContinuous
        .ColorIndex = 0
        .TintAndShade = 0
        .Weight = xlThin
    End With

    If Not confDrug Then Columns(COL_L_DRUG & ":" & COL_L_DRUG).Delete
    If Not confSick Then Columns(COL_L_SICK & ":" & COL_L_SICK).Delete
    
    Rows.AutoFit
    ActiveSheet.PageSetup.PrintArea = "1:" & wRow - 1

    '保存・後処理
    ActiveWorkbook.SaveAs curDir & "重複受診リスト.xlsx"
    ActiveWorkbook.Close
    shBasic.Delete
    If confSick Then shSick.Delete
    If confDrug Then shDrug.Delete
    Kill curDir & CSV_CONF_BASIC
    If confSick Then Kill curDir & CSV_CONF_SICK
    If confDrug Then Kill curDir & CSV_CONF_DRUG
    
End Sub

'進捗（数字）
Private Sub Prog(ByVal cnt As Long)
    If cnt Mod 10 = 0 Then
        DoEvents
        Application.StatusBar = cnt
    End If
End Sub

'進捗（文字）
Private Sub ProgStr(ByVal str As String)
    DoEvents
    Application.StatusBar = str
End Sub
