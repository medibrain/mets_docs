option explicit

dim arg,args
dim ast
dim strpath
dim fso
dim strex,strfn

set args=Wscript.Arguments
set fso=createobject("scripting.filesystemobject")
set ast=CreateObject("adodb.stream")

strpath=fso.getparentfoldername(WScript.ScriptFullName) 

with ast
	.mode=3
	.type=2
	'.charset="UTF-8"
	.charset="Shift_JIS"
	.open
	dim strRead

	for each arg in args
		.loadfromfile arg
		
		strRead=.ReadText
		
		
	next

	.close

end with
set ast=nothing

dim tmp
tmp=split(strRead,vbcrlf)
dim flgerr
dim outlog

dim r
flgerr=false
do while r<ubound(tmp)

	dim strKind
	strKind=left(tmp(r),4)
	
	dim flg
	
	
	

	if strKind= """DD"""  Then
		dim nextkind
		nextkind=left(tmp(r+1),4)
		if nextkind ="""DO""" then			
			outlog=outlog & "lineNo " &  r+1 & ":" & tmp(r) & vbcrlf
			flgerr=true
		end if
	end if
			
	
	r=r+1
loop




if flgerr=true then 	
	dim ws
	set ws=createobject("adodb.stream")
	with ws
		.type=2
		.charset="SHIFT_JIS"
		.open
		
		strpath=strpath & "\errLog_" & replace(replace(replace(formatdatetime(now),"/","")," ","_"),":","") & ".csv"
		.writetext(outlog)
		'.writetext(strRead)
		.savetoFile strpath,1
		.close
		
		
	end with

	set ws=nothing
end if

if flgerr=true then 	
	dim s
	set s=CreateObject("wscript.shell")
	s.run	"%windir%\notepad.exe " & strpath,1,false
else
	msgbox "no errors"
end if


